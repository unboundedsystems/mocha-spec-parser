declare module 'mocha/lib/ms' {
    export = ms;
    function ms(str: string): number;
    function ms(num: number): string;
}
