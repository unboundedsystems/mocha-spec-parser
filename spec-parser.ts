import * as  es from "event-stream";
import * as stream from "stream";
import * as sb from "stream-buffers";
import * as util from "util";
import * as builder from "xmlbuilder";
import Classifier, {
    Line,
    LineType,
    Output,
    Test,
    TestStatus
} from "./Classifier";

export interface SpecStats {
    passing: number;
    failing: number;
    pending: number;
}

export interface Spec {
    readonly stats: SpecStats;
    junitXML(): string;
}

export interface TestCaseAttrs {
    name: string;
    time?: number;
    pending?: boolean;
}

export function createParser(): Parser {
    return new ParserImpl();
}

class TestSuite {
    constructor(readonly name: string,
        readonly suites: TestSuite[],
        readonly testCases: Test[],
        readonly stats: SpecStats) {
    }

    toString(): string {
        const testCaseString = this.testCases.map((tc) => `"${tc}"`).join(",");
        const suitesString = this.suites.map((suite) => suite.toString()).join("\n,");
        return `
        { testCases: [${testCaseString}],
          suites: [${suitesString}] }
        `;
    }

    buildXML(xml: builder.XMLElementOrXMLNode) {
        const cur = xml.ele("testsuite", { name: this.name });
        cur.attribute("tests",
                      this.stats.passing +
                      this.stats.failing +
                      this.stats.pending);
        cur.attribute("failures", this.stats.failing);
        cur.attribute("pending", this.stats.pending);
        for (const suite of this.suites) {
            suite.buildXML(cur);
        }
        for (const tc of this.testCases) {
            const attrs: TestCaseAttrs = { name: tc.name };
            if (tc.status === TestStatus.pending) {
                attrs.pending = true;
            }
            attrs.time = (tc.durationInMs !== undefined) ?
                attrs.time = tc.durationInMs / 1000.0 :
                attrs.time = 0.0;

            const tcEle = cur.ele("testcase", attrs);
            if (tc.status === TestStatus.fail) {
                const failEle = tcEle.ele("failure", { type: "Error" });
                if (tc.failDiagnostic) {
                    failEle.cdata(tc.failDiagnostic);
                }
            }
        }
    }
}

function suiteFromLayer(cur: Layer): TestSuite {
    const nestedSuites: TestSuite[] = [];
    const testCases: Test[] = [];
    let passing = 0;
    let failing = 0;
    let pending = 0;
    for (const layer of cur.lower) {
        switch (layer.line.lineType) {
            case LineType.Suite:
                const nested = suiteFromLayer(layer);
                passing += nested.stats.passing || 0;
                failing += nested.stats.failing || 0;
                pending += nested.stats.pending || 0;
                nestedSuites.push(nested);
                break;
            case LineType.Test:
                const tc = layer.line;
                switch (tc.status) {
                    case TestStatus.fail:
                        failing++;
                        break;
                    case TestStatus.pass:
                        passing++;
                        break;
                    case TestStatus.pending:
                        pending++;
                        break;
                }
                testCases.push(tc);
                break;
            default:
                throw new Error(`Layer type ${layer.line.lineType} not understood`);
        }
    }
    if (cur.line.lineType !== LineType.Test &&
        cur.line.lineType !== LineType.Suite) {
        throw new Error(`Unexpected layer type ${cur.line.lineType}`);
    }
    return new TestSuite(cur.line.name,
        nestedSuites,
        testCases,
        { passing, failing, pending }
    );
}

class PostAmble {
    pass?: number;
    fail = 0;
    pending = 0;
    footnotes = new Map<number, string>();
}

function updateSuitesWithPostAmble(suites: TestSuite[], post: PostAmble) {
    for (const suite of suites) {
        for (const tc of suite.testCases) {
            if (tc.failNumber !== undefined) {
                tc.failDiagnostic = post.footnotes.get(tc.failNumber);
            }
        }
        updateSuitesWithPostAmble(suite.suites, post);
    }
}

class SpecImpl implements Spec {
    suites: TestSuite[];
    readonly stats: SpecStats;

    constructor(top: Layer, postAmble: PostAmble) {
        if (postAmble.pass === undefined) {
            throw new Error(`Unable to find test summary`);
        }

        let suites: TestSuite[] = [];
        for (const suite of top.lower) {
            suites.push(suiteFromLayer(suite));
        }

        this.stats = {
            failing: postAmble.fail,
            passing: postAmble.pass,
            pending: postAmble.pending,
        };

        // Mocha often creates a single, unnamed root suite that appears
        // as a blank line (but has the suite ANSI codes). Drop that because
        // it's confusing and not useful.
        if (suites.length === 1 &&
            suites[0].name === "" &&
            suites[0].testCases.length === 0) {
            suites = suites[0].suites;
        }

        updateSuitesWithPostAmble(suites, postAmble);

        this.suites = suites;
    }

    junitXML(): string {
        const xml = builder.create("testsuites");
        for (const suite of this.suites) {
            suite.buildXML(xml);
        }
        xml.end({ pretty: true });
        return xml.toString();
    }
    //FIXME(manishv) Needs branch and CI run info, date time, etc.
}

export interface Parser {
    parseSpec(ins: stream.Readable): Promise<Spec>;
}

class Layer {
    lower: Layer[];

    constructor(readonly line: Line,
        readonly depth: number,
        readonly prev: Layer | null) {
        this.lower = [];
    }
}

class Layers {
    readonly top: Layer;
    private cur: Layer;

    constructor() {
        this.top = new Layer(new Output("<<<top>>>", false), -1, null);
        this.cur = this.top;
    }

    append(line: Line, depth: number) {
        const l = new Layer(line, depth, this.cur);
        this.cur.lower.push(l);
    }

    push() {
        this.cur = this.cur.lower[this.cur.lower.length - 1];
    }

    pop() {
        if (this.cur.prev === null) {
            throw new Error("Layer pop past top");
        }
        this.cur = this.cur.prev;
    }

    popTo(depth: number) {
        while (this.cur.depth >= depth) {
            this.pop();
        }
    }
}

const lineBreakRE = /(?:\r\n)|\n/;

interface LineParser {
    next(lineInfo: Line): void;
    end(): void;
}

class PostAmbleParser implements LineParser {
    readonly postAmble: PostAmble = new PostAmble();
    private curNoteNum?: number;
    private curNote?: string[];
    private stopRecording = false;

    recordPendingNote() {
        if ((this.curNote !== undefined)
            && (this.curNoteNum !== undefined)) {
            //End of old note, start of new note
            this.postAmble.footnotes.set(this.curNoteNum,
                                         this.curNote.join("\n"));
            this.curNote = undefined;
            this.curNoteNum = undefined;
        }
    }

    end() {
        this.recordPendingNote();
    }

    next(lineInfo: Line) {
        if (this.stopRecording) return;

        switch (lineInfo.lineType) {
            case LineType.Summary:
                this.postAmble[lineInfo.status] = lineInfo.count;
                break;

            case LineType.FailureHeader:
                // Finalize the last note
                this.recordPendingNote();
                // Start a new note
                this.curNoteNum = lineInfo.failNumber;
                this.curNote = [lineInfo.text];
                break;

            // FIXME(mark): We really should start a new Spec, but for the
            // moment, just give a warning so we can see how many projects
            // have this issue.
            case LineType.Suite:
                if (!this.stopRecording) {
                    // tslint:disable-next-line:no-console
                    console.log(`WARNING: Found a second mocha test run`);
                    this.stopRecording = true;
                }
                break;

            default:
            case LineType.Output:
                // Ignore any lines that happen before the first FailureHeader
                if (this.curNote) {
                    this.curNote.push(lineInfo.text);
                }
                break;
        }
    }
}

class LayerParser implements LineParser {
    readonly layers = new Layers();
    public testCount = 0; // Consistency check
    private indentLevel = -1;

    // tslint:disable-next-line:no-empty
    end() { }

    //Returns true line if postAmble start
    next(lineInfo: Line): void {

        switch (lineInfo.lineType) {
            case LineType.Output:
                return;  // no action

            case LineType.Test:
                this.testCount++;
                // Fall through
            case LineType.Suite:
                const newIndentLevel = lineInfo.indent.length;
                switch (true) {
                    case this.indentLevel < 0:
                        break;
                    case newIndentLevel > this.indentLevel:
                        this.layers.push();
                        break;
                    case newIndentLevel < this.indentLevel:
                        this.layers.popTo(newIndentLevel);
                        break;
                }
                this.indentLevel = newIndentLevel;
                this.layers.append(lineInfo, this.indentLevel);
                return;
        }
    }
}

//Only exported for testing
class ParserImpl implements Parser {
    parseSpec(ins: stream.Readable): Promise<Spec> {
        return new Promise<Spec>((resolve, reject) => {
            const layerParser = new LayerParser();
            const postAmbleParser = new PostAmbleParser();
            let layerParserDone = false;

            const classifier = new Classifier((lineInfo, inPostAmble) => {
                if (inPostAmble) {
                    if (!layerParserDone) {
                        layerParser.end();
                        layerParserDone = true;
                    }

                    try {
                        postAmbleParser.next(lineInfo);
                    } catch (e) {
                        reject(e);
                        ins.unpipe();
                    }
                } else {
                    layerParser.next(lineInfo);
                }
            });

            ins.pipe(es.split(lineBreakRE))
                .pipe(es.mapSync((line: string): void => {
                    classifier.next(line);
                }));
            ins.on("end", () => {
                classifier.end();
                postAmbleParser.end();

                // Check that parser actually parsed the right number of test
                // cases.
                const pa = postAmbleParser.postAmble;
                const parsed = layerParser.testCount;
                const summary = pa.fail + (pa.pass || 0) + (pa.pending || 0);
                if (parsed !== summary) {
                    // tslint:disable-next-line:no-console
                    console.log(`WARNING: Parsed ${parsed} test cases, but summary has ${summary}.`);
                }

                try {
                    resolve(new SpecImpl(layerParser.layers.top, postAmbleParser.postAmble));
                } catch (e) {
                    reject(e);
                }
            });
        });
    }
}
