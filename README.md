This module attempts to parse a mocha spec test log and convert it
into various formats, for now JUnit XML format with extensions for
nested suites.

```
const parser = createParser();
const spec = parser.parseSpec(process.stdin);
const xmlString = spec.junitXML();
console.log(xmlString);
```