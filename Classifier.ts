import ms = require("mocha/lib/ms");
import { defaultSymbols, defaultWinSymbols } from "./spec-info";

const debug = false;

export enum TestStatus {
    pass = "pass",
    fail = "fail",
    pending = "pending",
}
export type Speed = "slow" | "medium" | "fast";

const lineEnd = "$";

export enum LineType {
    FailureHeader = "FailureHeader",
    Output        = "Output",
    Suite         = "Suite",
    Summary       = "Summary",
    Test          = "Test",
}
export type Line = FailureHeader | Output | Suite | Summary | Test;

export interface LineBase {
    readonly lineType: LineType;
    readonly text: string;
    readonly decisive: boolean;
    lineNo?: number;
}

export enum ParseType {
    Color = "color",
    Plain = "plain",
}

export type LineMatcherFunc = (line: string, parseType: ParseType) => Line | null;
export interface LineMatcher {
    match: LineMatcherFunc;
    notInPostAmble: boolean;
}

// In Plain mode, there's no way to disambiguate between typical output and
// a suite name with zero indent. So enforce a minimum indent. That might mean
// we miss suite names with zero indent, but in practice, mocha seems to always
// have at least one indent (default 2 spaces).
function suiteRE(cFunc: ColorFunc, minIndent: string) {
    return RegExp(
        "^"
        + cFunc("\\d+") + "(" + minIndent + " *)(.*)" + cFunc("0")
        + lineEnd);
}

export class Suite implements LineBase {
    static notInPostAmble = true;
    static regex = {
        color: suiteRE(color, ""),
        plain: suiteRE(noColor, "  "),
    };

    static match(line: string, parseType: ParseType): Suite | null {
        if (line === "") return null;

        const m = Suite.regex[parseType].exec(line);
        if (m) {
            // With the ANSI color codes, we're certain about the parse
            return new Suite(line, m[1], m[2], parseType === ParseType.Color);
        }
        return null;
    }

    readonly lineType = LineType.Suite;
    lineNo?: number;

    constructor(
        readonly text: string,
        readonly indent: string,
        readonly name: string,
        readonly decisive: boolean,
    ) {
    }

}

function passRe(cFunc: ColorFunc, checkmark: string) {
    return RegExp(
        "^"
        + "(?:" // non-capturing group for ANSI codes in mocha v2 output
        + "\u001b\\[2K\u001b\\[0G"
        + ")?"  // ANSI group is optional

        + "( *)"                             // indent = $1
        + cFunc("\\d+") + "  " + checkmark + cFunc("0") // colored checkmark
        + cFunc("\\d+")                      // title color
        + " (.*?)"                           // test title = $2
        + cFunc("0")                         // end title color
        + "(?:" // non-capturing group for slow tests
            + cFunc("\\d+") + " \\((\\d+)ms\\)" + cFunc("0") // duration = $3
        + ")?"  // slow group is optional
        + lineEnd);
}

function failRe(cFunc: ColorFunc) {
    return RegExp(
        "^"
        + "(?:" // non-capturing group for ANSI codes in mocha v2 output
        + "\u001b\\[2K\u001b\\[0G"
        + ")?"  // ANSI group is optional

        + "( *)"             // indent = $1
        + cFunc("\\d+")      // fail color
        + "  (\\d+)\\) (.*)" // failure number = $2 & test title = $3
        + cFunc("0")         // end fail color
        + lineEnd);
}

function pendingRe(cFunc: ColorFunc) {
    return RegExp(
        "^( *)"            // indent = $1
        + cFunc("\\d+")    // pending color
        + "  - (.*)"       // test title = $2
        + cFunc("0")       // end pending color
        + lineEnd);
}

export class Test implements LineBase {
    static notInPostAmble = true;

    static pass = {
        color: passRe(color, "."), // Any ok symbol is fine
        plain: passRe(noColor, `(?:${defaultSymbols.ok}|${defaultWinSymbols.ok})`),
    };
    static fail = {
        color: failRe(color),
        plain: failRe(noColor),
    };
    static pending = {
        color: pendingRe(color),
        plain: pendingRe(noColor),
    };

    static match(line: string, parseType: ParseType): Test | null {
        // NOTE: total indent is the variable amount of indent from $1 PLUS
        //       the two spaces that go with the checkmark/X/-
        let m = Test.pass[parseType].exec(line);
        if (m) {
            // Test pass is ALWAYS decisive, either due to ANSI colors OR
            // the checkmark.
            return new Test(line, m[1] + "  ", m[2], TestStatus.pass, true,
                            undefined, toNumIfDef(m[3]));
        }

        m = Test.fail[parseType].exec(line);
        if (m) {
            // Test fail is ALWAYS decisive, either due to ANSI colors OR
            // the indent/number/right paren pattern. This is definitely
            // a weaker check than test pass.
            return new Test(line, m[1] + "  ", m[3], TestStatus.fail, true,
                            toNumIfDef(m[2]));
        }

        m = Test.pending[parseType].exec(line);
        if (m) {
            // With the ANSI color codes, we're certain about the parse
            return new Test(line, m[1] + "  ", m[2], TestStatus.pending,
                            parseType === ParseType.Color);
        }

        return null;
    }

    readonly lineType = LineType.Test;
    lineNo?: number;
    failDiagnostic?: string;

    constructor(
        readonly text: string,
        readonly indent: string,
        readonly name: string,
        readonly status: TestStatus,
        readonly decisive: boolean,
        readonly failNumber?: number,
        readonly durationInMs?: number,
    ) {}
}

function passingSumRe(cFunc: ColorFunc) {
    return RegExp("^"
        + cFunc("\\d+") + " " + cFunc("0")
        + cFunc("\\d+") + " (\\d+) passing" + cFunc("0") // count = $1
        + cFunc("\\d+") + " \\((\\d.*)\\)" + cFunc("0")        // duration = $2
        + lineEnd
    );
}

function pendingSumRe(cFunc: ColorFunc) {
    return RegExp("^"
        + cFunc("\\d+") + " " + cFunc("0")
        + cFunc("\\d+") + " (\\d+) pending" + cFunc("0") // count = $1
        + lineEnd);
}

function failingSumRe(cFunc: ColorFunc) {
    return RegExp("^"
        + cFunc("\\d+") + "  (\\d+) failing" + cFunc("0") // count = $1
        + lineEnd);
}

export class Summary implements LineBase {
    static notInPostAmble = false;

    static passing = {
        color: passingSumRe(color),
        plain: passingSumRe(noColor),
    };
    static pending = {
        color: pendingSumRe(color),
        plain: pendingSumRe(noColor),
    };
    static failing = {
        color: failingSumRe(color),
        plain: failingSumRe(noColor),
    };

    static match(line: string, parseType: ParseType): Summary | null {
        let m = Summary.passing[parseType].exec(line);
        if (m) {
            // Both color and non color are decisive
            return new Summary(line, TestStatus.pass, true,
                               toNum(m[1]), ms(m[2]));
        }

        m = Summary.failing[parseType].exec(line);
        if (m) {
            // Both color and non color are decisive
            return new Summary(line, TestStatus.fail, true, toNum(m[1]));
        }

        m = Summary.pending[parseType].exec(line);
        if (m) {
            // Both color and non color are decisive
            return new Summary(line, TestStatus.pending, true, toNum(m[1]));
        }

        return null;
    }

    readonly lineType = LineType.Summary;
    lineNo?: number;

    constructor(
        readonly text: string,
        readonly status: TestStatus,
        readonly decisive: boolean,
        readonly count: number,
        readonly durationInMs?: number) {
    }
}

function failHeaderRe(cFunc: ColorFunc) {
    return RegExp("^"
        + cFunc("\\d+")
        + "  (\\d+)\\) (.*):"  // failNum = $1, testName = $2
        + lineEnd);
}

export class FailureHeader implements LineBase {
    static notInPostAmble = false;

    static regex = {
        color: failHeaderRe(color),
        plain: failHeaderRe(noColor),
    };

    static match(line: string, parseType: ParseType): FailureHeader | null {
        const m = FailureHeader.regex[parseType].exec(line);
        if (!m) return null;

        return new FailureHeader(line, toNum(m[1]), m[2], true);
    }

    readonly lineType = LineType.FailureHeader;
    lineNo?: number;

    constructor(
        readonly text: string,
        readonly failNumber: number,
        readonly name: string,
        readonly decisive: boolean,
    ) {
    }

}

export class Output implements LineBase {
    static notInPostAmble = false;

    static match(line: string, parseType: ParseType): Output | null {
        // Output is never decisive
        return new Output(line, false);
    }

    readonly lineType = LineType.Output;
    lineNo?: number;

    constructor(readonly text: string, readonly decisive: boolean) {}

}

const classifiers: LineMatcher[] = [
    Summary,
    Test,
    FailureHeader,
    Suite, // Suite will also match Test and Summary, so Suite goes after
    Output // Must be last
];

function printLine(line: Line, info = "") {
    if (debug) {
        // tslint:disable-next-line:no-console
        console.log(`${info}${line.lineNo || "?"} (${line.lineType}): ${line.text}`);
    }
}

export type ClassifierCallback = (lineInfo: Line, inPostAmble: boolean) => void;

export default class Classifier {
    inPostAmble = false;
    lineNo = 0;
    uncertainLines: string[] = [];
    certain = false;

    private parseTypes = [
        ParseType.Color,
        ParseType.Plain,
    ];

    constructor(private callback: ClassifierCallback) {
    }

    next(line: string): void {
        let m: Line | null = null;
        let pt: ParseType | null = null;

        for (pt of this.parseTypes) {
            m = this.matchOne(line, pt);
            if (m) {
                if (this.certain || m.decisive) {
                    // We know for sure which ParseType we're using, so no
                    // need to check other ParseTypes
                    this.sendOrQueue(m, pt);
                    return;
                }
            }
        }
        if (!m || !pt) {
            throw new Error(`Internal error. Last classifier should have matched`);
        }
        this.sendOrQueue(m, pt);
    }

    end(): void {
        this.sendQueued();
    }

    private matchOne(line: string, parseType: ParseType) {
        for (const c of classifiers) {
            // If we're in the postamble and this classifier type doesn't
            // occur in the postamble, don't match against it.
            if (this.inPostAmble && c.notInPostAmble) continue;

            const m = c.match(line, parseType);
            if (m) return m;
        }
        return null;
    }

    private send(line: Line) {
        // The first summary line begins the postamble
        if (line.lineType === LineType.Summary) {
            this.inPostAmble = true;
        }

        line.lineNo = ++this.lineNo;
        printLine(line);
        this.callback(line, this.inPostAmble);
    }

    private sendOrQueue(line: Line, parseType: ParseType) {
        if (this.certain) {
            this.send(line);
            return;
        }

        printLine(line, "Q:");
        this.uncertainLines.push(line.text);

        if (line.decisive) {
            // Reduce to a single parse type
            this.parseTypes = [parseType];
            this.certain = true;

            // Process any queued lines
            this.sendQueued();
        }
    }

    private sendQueued() {
        for (const l of this.uncertainLines) {
            const m = this.matchOne(l, this.parseTypes[0]);
            if (!m) {
                throw new Error(`Internal error. Last classifier should have matched!`);
            }
            this.send(m);
        }
        this.uncertainLines = [];
    }

}

type ColorFunc = (which: string) => string;
function color(which: string): string {
    return `\u001b\\[${which}m`;
}
function noColor(which: string): string {
    return "";
}
function toNumIfDef(str: string): number | undefined {
    return str ? toNum(str) : undefined;
}
function toNum(str: string): number {
    const n = parseInt(str, 10);
    return isNaN(n) ? 0 : n;
}
