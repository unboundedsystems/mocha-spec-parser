export const defaultColors = {
    "bright fail": 91,
    "bright pass": 92,
    "bright yellow": 93,
    "checkmark": 32,
    "diff added": 32,
    "diff gutter": 90,
    "diff removed": 31,
    "error message": 31,
    "error stack": 90,
    "error title": 0,
    "fail": 31,
    "fast": 90,
    "green": 32,
    "light": 90,
    "medium": 33,
    "pass": 90,
    "pending": 36,
    "slow": 31,
    "suite": 0,
};
export type Colors = typeof defaultColors;

/**
 * Default symbol map.
 */
export const defaultSymbols = {
    bang: "!",
    comma: ",",
    dot: "․",
    err: "✖",
    ok: "✓",
};
export type Symbols = typeof defaultSymbols;

export const defaultWinSymbols = {
    ...defaultSymbols,
    dot: ".",
    err: "\u00D7",
    ok:  "\u221A",
};

export interface Config {
    colors: Colors | null;
    symbols: Symbols;
    indentSize: number;
    lineEnd: string;
    isatty: boolean;
    // Prior to mocha v3.0.0, there were extra calls to CR() before each
    // passing and failing test. See mocha commit 87415
    mocha2cr: boolean;
}

export const defaultConfig: Config = {
    colors: defaultColors,
    indentSize: 2,
    isatty: true,
    lineEnd: "\n",
    mocha2cr: false,
    symbols: defaultSymbols,
};
export const defaultWinConfig: Config = {
    ...defaultConfig,
    lineEnd: "\r\n",
    symbols: defaultWinSymbols,
};
export const noColorsConfig: Config = {
    ...defaultConfig,
    colors: null,
};
