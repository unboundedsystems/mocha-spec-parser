import * as fs from "fs";
import * as sb from "stream-buffers";
import stripAnsi = require("strip-ansi");
import * as util from "util";
import * as parser from "../spec-parser";
import {
  testFail,
  testPass,
  testPending,
} from "./Classifier.spec";
import {
  mkEpilogue,
  mkSuite,
  mkTest,
  TestResult,
} from "./utils";

// Turn off the no-trailing-whitespace lint rule due to the purposeful
// embedded spaces within some of the test output comparison strings.
/* tslint:disable:no-trailing-whitespace */

function buildStream(ins: sb.ReadableStreamBuffer, data: string) {
  process.nextTick(() => {
    ins.put(data);
    ins.stop();
  });

}

async function sleep(ms: number): Promise<void> {
  return new Promise<void>((resolve, reject) => {
    setTimeout(() => resolve(), ms);
  });
}

function xml(spec: parser.Spec): string {
  return stripAnsi(spec.junitXML().trim());
}

describe("SpecParser Basic Tests", () => {
  let ins: sb.ReadableStreamBuffer;
  let prsr: parser.Parser;

  beforeEach(async () => {
    //Use chunkSize 13 to test streaming
    ins = new sb.ReadableStreamBuffer({ chunkSize: 13 });
    prsr = parser.createParser();
  });

  it("Instatiate", () => {
    should(prsr).not.be.Null();
  });

  it("Should parse 1 top-level suite, 0 test cases", async () => {
    const suiteName = "This is a suite";
    const suite =
      mkSuite(suiteName, 0) +
      mkEpilogue({passes: 0, duration: 20});
    buildStream(ins, suite);
    const spec = await prsr.parseSpec(ins);
    should(xml(spec)).equal(
      `<testsuites>
  <testsuite name="${suiteName}" tests="0" failures="0" pending="0"/>
</testsuites>`);
  });

  it("Should parse 1 top-level suite, 1 test case", async () => {
    const suiteName = "This is a suite";
    const testCaseName = "This is a test case";
    const suite =
      mkSuite(suiteName, 0) +
      mkTest({...testPass, title: testCaseName}, 1) +
      mkEpilogue({passes: 1, duration: 20});
    buildStream(ins, suite);
    const spec = await prsr.parseSpec(ins);
    should(xml(spec)).equal(
      `<testsuites>
  <testsuite name="${suiteName}" tests="1" failures="0" pending="0">
    <testcase name="${testCaseName}" time="0"/>
  </testsuite>
</testsuites>`);
    should(spec.stats.passing).equal(1);
    should(spec.stats.failing).equal(0);
  });

  it("Should parse 1 top-level suite, 1 failed test case", async () => {
    const suiteName = "This is a suite";
    const testCaseName = "This is a test case";
    const failures = [
      {name: testCaseName, failNum: 11},
    ];
    const suite =
      mkSuite(suiteName, 0) +
      mkTest({...testFail, title: testCaseName, failNumber: 11}, 1) +
      mkEpilogue({passes: 0, failures, duration: 20});

    buildStream(ins, suite);
    const spec = await prsr.parseSpec(ins);
    should(xml(spec)).equal(
      // NOTE! There are 5 spaces purposely embedded in the second line of
      // the CDATA section below. Mocha ouputs all those line feeds and all
      // those spaces in real output.
      `<testsuites>
  <testsuite name="${suiteName}" tests="1" failures="1" pending="0">
    <testcase name="${testCaseName}" time="0">
      <failure type="Error">
        <![CDATA[  11) ${testCaseName}:
     




]]>
      </failure>
    </testcase>
  </testsuite>
</testsuites>`);
    should(spec.stats.passing).equal(0);
    should(spec.stats.failing).equal(1);
    should(spec.stats.pending).equal(0);
  });

  it("Should parse 1 top-level suite, 1 pending, 1 passing test case", async () => {
    const suiteName = "This is a suite";
    const testCaseName1 = "This is a test case";
    const testCaseName2 = "This is a pending test case";
    const suite =
      mkSuite(suiteName, 0) +
      mkTest({...testPass, title: testCaseName1}, 1) +
      mkTest({...testPending, title: testCaseName2}, 1) +
      mkEpilogue({passes: 1, pending: 1, duration: 20});
    buildStream(ins, suite);
    const spec = await prsr.parseSpec(ins);
    should(xml(spec)).equal(
      `<testsuites>
  <testsuite name="${suiteName}" tests="2" failures="0" pending="1">
    <testcase name="${testCaseName1}" time="0"/>
    <testcase name="${testCaseName2}" pending="true" time="0"/>
  </testsuite>
</testsuites>`);
    should(spec.stats.passing).equal(1);
    should(spec.stats.pending).equal(1);
    should(spec.stats.failing).equal(0);
  });

  it("Should parse 2 top-level suites, 2 passing test cases, 2 failed test cases", async () => {
    const suiteName1 = "This is a suite 1";
    const suiteName2 = "This is a suite 2";
    const testCaseName1 = "This is a failing test case";
    const testCaseName2 = "This is a passing test case";
    const testCaseName3 = "This is another failing test case";
    const testCaseName4 = "This is another passing test case";
    const failures = [
      {name: testCaseName1},
      {name: testCaseName3},
    ];
    const suite =
      mkSuite(suiteName1, 0) +
      mkTest({...testFail, title: testCaseName1, failNumber: 1}, 1) +
      mkTest({...testPass, title: testCaseName2}, 1) +
      mkSuite(suiteName2, 0) +
      mkTest({...testFail, title: testCaseName3, failNumber: 2}, 1) +
      mkTest({...testPass, title: testCaseName4}, 1) +
      mkEpilogue({passes: 2, failures, duration: 20});
    buildStream(ins, suite);
    const spec = await prsr.parseSpec(ins);
    should(xml(spec)).equal(
      `<testsuites>
  <testsuite name="${suiteName1}" tests="2" failures="1" pending="0">
    <testcase name="${testCaseName1}" time="0">
      <failure type="Error">
        <![CDATA[  1) ${testCaseName1}:
     

]]>
      </failure>
    </testcase>
    <testcase name="${testCaseName2}" time="0"/>
  </testsuite>
  <testsuite name="${suiteName2}" tests="2" failures="1" pending="0">
    <testcase name="${testCaseName3}" time="0">
      <failure type="Error">
        <![CDATA[  2) ${testCaseName3}:
     




]]>
      </failure>
    </testcase>
    <testcase name="${testCaseName4}" time="0"/>
  </testsuite>
</testsuites>`);
    should(spec.stats.passing).equal(2);
    should(spec.stats.failing).equal(2);
  });

  it("Should parse 1 top-level suites, 2 passing test cases, 2 failed test cases", async () => {
    const suiteName = "This is a suite";
    const testCaseName1 = "This is a failing test case";
    const testCaseName2 = "This is a passing test case";
    const testCaseName3 = "This is another failing test case";
    const testCaseName4 = "This is another passing test case";
    const failures = [
      {name: testCaseName1},
      {name: testCaseName3},
    ];
    const suite =
      mkSuite(suiteName, 0) +
      mkTest({...testFail, title: testCaseName1, failNumber: 1}, 1) +
      mkTest({...testPass, title: testCaseName2}, 1) +
      mkTest({...testFail, title: testCaseName3, failNumber: 2}, 1) +
      mkTest({...testPass, title: testCaseName4}, 1) +
      mkEpilogue({passes: 2, failures, duration: 20});
    buildStream(ins, suite);
    const spec = await prsr.parseSpec(ins);
    should(xml(spec)).equal(
      `<testsuites>
  <testsuite name="${suiteName}" tests="4" failures="2" pending="0">
    <testcase name="${testCaseName1}" time="0">
      <failure type="Error">
        <![CDATA[  1) ${testCaseName1}:
     

]]>
      </failure>
    </testcase>
    <testcase name="${testCaseName2}" time="0"/>
    <testcase name="${testCaseName3}" time="0">
      <failure type="Error">
        <![CDATA[  2) ${testCaseName3}:
     




]]>
      </failure>
    </testcase>
    <testcase name="${testCaseName4}" time="0"/>
  </testsuite>
</testsuites>`);
    should(spec.stats.passing).equal(2);
    should(spec.stats.failing).equal(2);
  });

  it("Should parse even when top-level is indented", async () => {
    const suiteName1 = "This is a suite 1";
    const suiteName2 = "This is a suite 2";
    const testCaseName1 = "This is a failing test case";
    const testCaseName2 = "This is a passing test case";
    const testCaseName3 = "This is another failing test case";
    const testCaseName4 = "This is another passing test case";
    const failures = [
      {name: testCaseName1},
      {name: testCaseName3},
    ];
    const suite =
      mkSuite(suiteName1, 1) +
      mkTest({...testFail, title: testCaseName1, failNumber: 1}, 2) +
      mkTest({...testPass, title: testCaseName2}, 2) +
      mkSuite(suiteName2, 1) +
      mkTest({...testFail, title: testCaseName3, failNumber: 2}, 2) +
      mkTest({...testPass, title: testCaseName4}, 2) +
      mkEpilogue({passes: 2, failures, duration: 20});
    buildStream(ins, suite);
    const spec = await prsr.parseSpec(ins);
    should(xml(spec)).equal(
      `<testsuites>
  <testsuite name="${suiteName1}" tests="2" failures="1" pending="0">
    <testcase name="${testCaseName1}" time="0">
      <failure type="Error">
        <![CDATA[  1) ${testCaseName1}:
     

]]>
      </failure>
    </testcase>
    <testcase name="${testCaseName2}" time="0"/>
  </testsuite>
  <testsuite name="${suiteName2}" tests="2" failures="1" pending="0">
    <testcase name="${testCaseName3}" time="0">
      <failure type="Error">
        <![CDATA[  2) ${testCaseName3}:
     




]]>
      </failure>
    </testcase>
    <testcase name="${testCaseName4}" time="0"/>
  </testsuite>
</testsuites>`);
    should(spec.stats.passing).equal(2);
    should(spec.stats.failing).equal(2);
  });

  it("Should reject promise on test case parse errors", () => {
    buildStream(ins, `Suite
  InvalidStatus Here is a test
`);

    return should(prsr.parseSpec(ins)).rejectedWith(Error);
  });

  it("Should reject promise on post amble parse errors", () => {
    buildStream(ins, `
Suite
  1) Failing test

0 passing
Illegal text here
    `);

    return should(prsr.parseSpec(ins)).rejectedWith(Error, /summary/i);
  });
});

describe("SpecParser Real Examples", () => {
  let ins: sb.ReadableStreamBuffer;
  let prsr: parser.Parser;

  beforeEach(async () => {
    //Use chunkSize 13 to test streaming
    ins = new sb.ReadableStreamBuffer({ chunkSize: 13 });
    prsr = parser.createParser();
  });

  it("Should parse mysql-transit-spec-output-1.txt", async () => {
    const raw = fs.readFileSync("test/mysql-transit-spec-output-1.txt").toString();
    buildStream(ins, raw);
    const spec = await prsr.parseSpec(ins);
    should(xml(spec)).equal(
      `<testsuites>
  <testsuite name="When run mysql transit non interactive" tests="1" failures="0" pending="0">
    <testsuite name="and there is a new table in temp database" tests="1" failures="0" pending="0">
      <testcase name="it should be replicated also in the original database" time="0"/>
    </testsuite>
  </testsuite>
  <testsuite name="When run mysql transit non interactive" tests="1" failures="0" pending="0">
    <testsuite name="and there is a new column in temp database" tests="1" failures="0" pending="0">
      <testcase name="it should be replicated also in the original database" time="0"/>
    </testsuite>
  </testsuite>
  <testsuite name="When run mysql transit non interactive" tests="1" failures="0" pending="0">
    <testsuite name="and the type of a column has changes in temp database" tests="1" failures="0" pending="0">
      <testcase name="it should be replicated also in the original database" time="0"/>
    </testsuite>
  </testsuite>
  <testsuite name="When run mysql transit non interactive" tests="1" failures="0" pending="0">
    <testsuite name="and a column is dropped in temp database" tests="1" failures="0" pending="0">
      <testcase name="it should be still available in the original database" time="0"/>
    </testsuite>
  </testsuite>
  <testsuite name="When run mysql transit non interactive" tests="1" failures="0" pending="0">
    <testsuite name="and a table is dropped in temp database" tests="1" failures="0" pending="0">
      <testcase name="it should be still available in the original database" time="0"/>
    </testsuite>
  </testsuite>
</testsuites>`);
    should(spec.stats.passing).equal(5);
    should(spec.stats.failing).equal(0);
  });

  it("Should parse mocha-spec-parser-spec-output-with-failure.txt", async () => {
    const raw = fs.readFileSync("test/mocha-spec-parser-spec-output-with-failure.txt").toString();
    buildStream(ins, raw);
    const spec = await prsr.parseSpec(ins);
    /* tslint:disable:max-line-length */
    should(xml(spec)).equal(
      `<testsuites>
  <testsuite name="SpecParser Basic Tests" tests="5" failures="1" pending="0">
    <testcase name="Instatiate" time="0"/>
    <testcase name="Should parse empty string" time="0"/>
    <testcase name="Should parse 1 top-level suite, 0 test cases" time="0"/>
    <testcase name="Should parse 1 top-level suite, 1 test case" time="0"/>
    <testcase name="Should parse 1 top-level suite, 1 failed test case" time="0">
      <failure type="Error">
        <![CDATA[  1) SpecParser Basic Tests:
       Should parse 1 top-level suite, 1 failed test case:

      AssertionError: expected '<testsuites>\\n  <testsuite name="This is a suite" tests="2" failures="1">\\n    <testcase name="This is a test case" time="0"/>\\n  </testsuite>\\n</testsuites>' to be '<testsuites>\\n  <testsuite name="This is a suite">\\n    <testcase name="This is a test case" time="0">\\n        <failure type="Error">\\n          Fail message.\\n        </failure>\\n    </testcase>\\n  </testsuite>\\n</testsuites>'
      + expected - actual

       <testsuites>
      -  <testsuite name="This is a suite" tests="2" failures="1">
      -    <testcase name="This is a test case" time="0"/>
      +  <testsuite name="This is a suite">
      +    <testcase name="This is a test case" time="0">
      +        <failure type="Error">
      +          Fail message.
      +        </failure>
      +    </testcase>
         </testsuite>
       </testsuites>
      
      at Assertion.fail (node_modules/should/cjs/should.js:275:17)
      at Assertion.value (node_modules/should/cjs/should.js:356:19)
      at Object.<anonymous> (test/spec-parser.spec.ts:75:40)
      at Generator.next (<anonymous>)
      at fulfilled (dist/test/spec-parser.spec.js:4:58)
      at <anonymous>
      at process._tickCallback (internal/process/next_tick.js:188:7)


]]>
      </failure>
    </testcase>
  </testsuite>
</testsuites>`);
    /* tslint:enable:max-line-length */
    should(spec.stats.passing).equal(4);
    should(spec.stats.failing).equal(1);

  });

  it("Should parse mochawesome-report-generator-log-1.txt", async function() {
    this.timeout(9000);
    const raw = fs.readFileSync("test/mochawesome-report-generator-log-1.txt").toString();
    const xmlOut = fs.readFileSync("test/mochawesome-report-generator-log-1.xml").toString().trim();
    buildStream(ins, raw);
    const spec = await prsr.parseSpec(ins);
    should(xml(spec)).equal(xmlOut);
  });

  it("Should parse node-htsengine-log-1.txt", async function() {
    this.timeout(4000);
    const raw = fs.readFileSync("test/node-htsengine-log-1.txt").toString();
    const xmlOut = fs.readFileSync("test/node-htsengine-log-1.xml").toString().trim();
    buildStream(ins, raw);
    const spec = await prsr.parseSpec(ins);
    should(xml(spec)).equal(xmlOut);
  });

  it("Should parse CodeceptJS-log-1.log (Plain, second mocha)", async () => {
    const raw = fs.readFileSync("test/CodeceptJS-log-1.txt").toString();
    const xmlOut = fs.readFileSync("test/CodeceptJS-log-1.xml").toString().trim();
    buildStream(ins, raw);
    const spec = await prsr.parseSpec(ins);
    should(xml(spec)).equal(xmlOut);
  });

  it("Should parse CodeceptJS-log-2.log (Plain)", async () => {
    const raw = fs.readFileSync("test/CodeceptJS-log-2.txt").toString();
    const xmlOut = fs.readFileSync("test/CodeceptJS-log-2.xml").toString().trim();
    buildStream(ins, raw);
    const spec = await prsr.parseSpec(ins);
    should(xml(spec)).equal(xmlOut);
  });

});
