import Classifier, {
  ClassifierCallback,
  Line,
  LineMatcher,
  LineType,
  Output,
  ParseType,
  Suite,
  Summary,
  Test,
  TestStatus
} from "../Classifier";
import { Config, defaultConfig, defaultWinConfig } from "../spec-info";
import { mkEpilogue, mkSuite, mkTest, TestResult } from "./utils";

export const testPass: TestResult = {
  title: "a test",
  status: "pass",
  speed: "fast",
  duration: 20,
};
export const testFail: TestResult = {
  title: "a failing test",
  status: "fail",
  speed: "fast",
  duration: 20,
  failNumber: 1,
};
export const testSlow: TestResult = {
  title: "a slow test",
  status: "pass",
  speed: "slow",
  duration: 1234,
};
export const testPending: TestResult = {
  title: "a pending test",
  status: "pending",
  speed: "fast",
  duration: 20,
};

function parameterizedSuite<T>(
  description: string, dataArray: T[],
  callback: (this: Mocha.ISuiteCallbackContext, data: T) => void
): void {
  let d: any;
  let count = 0;

  for (d of dataArray) {
    count++;
    const desc = description + ": " + d.id || count.toString();
    describe(desc, function(this: Mocha.ISuiteCallbackContext) {
      callback.call(this, d);
    });
  }
}

const configs = [
  {id: "default config", cfg: {...defaultConfig, lineEnd: ""} },
  {id: "default Win config", cfg: {...defaultWinConfig, lineEnd: ""} },
  {id: "mocha 2 config", cfg: {...defaultConfig, lineEnd: "", mocha2cr: true} },
  {id: "no color config", cfg: {...defaultConfig, lineEnd: "", colors: null} },
];

function matchClassifer(line: string, parseType: ParseType): Line | null {
  let ret: Line | null = null;
  const classifier = new Classifier((lInfo) => {
    // Last line is what will be returned
    ret = lInfo;
  });

  // If parseType is Plain, prime the Classifier with a decisive line first
  // or everything ends up as Output, which isn't useful for testing.
  if (parseType === ParseType.Plain) {
    classifier.next(mkTest(testPass, 0,
      {...defaultConfig, lineEnd: "", colors: null}));
  }

  classifier.next(line);
  classifier.end();
  return ret!;
}

parameterizedSuite("Mocha output format", configs, (data) => {
  const cfg = data.cfg;
  const pType = data.cfg.colors ? ParseType.Color : ParseType.Plain;

  // Run tests with the lower level Suite.match AND with Classifier.next
  const suiteMatchers = [
    { id: "Suite",      match: Suite.match },
    { id: "Classifier", match: matchClassifer }
  ];

  parameterizedSuite('Classify "Suite" Tests', suiteMatchers, (p) => {

    it("should not match empty", () => {
      const res = Suite.match("", pType);
      should(res).equal(null);
    });

    // Can't detect an empty suite name in Plain mode, but can in Color
    if (pType === ParseType.Color) {
      it("should match empty suite name", () => {
        const line = mkSuite("", 0, cfg);
        const res = p.match(line, pType);
        should(res).not.equal(null);
        if (!res) return;

        res.text.should.equal(line);
        res.lineType.should.equal(LineType.Suite);
        if (res.lineType === LineType.Suite) {
          res.indent.should.equal("");
          res.name.should.equal("");
        }
      });
    }

    const minIndent = pType === ParseType.Color ? 0 : 1;

    it("should match minimum indent", () => {
      const suiteName = "This is a suite name";
      const line = mkSuite(suiteName, minIndent, cfg);
      const res = p.match(line, pType);
      should(res).not.equal(null);
      if (!res) return;

      res.text.should.equal(line);
      res.lineType.should.equal(LineType.Suite);
      if (res.lineType === LineType.Suite) {
        res.indent.should.equal(" ".repeat(minIndent * cfg.indentSize));
        res.name.should.equal(suiteName);
      }
    });

    it("should match with indent", () => {
      const suiteName = "This is a suite name";
      const line = mkSuite(suiteName, 1, cfg); // indent=1 -> 2 spaces
      const res = p.match(line, pType);
      should(res).not.equal(null);
      if (!res) return;

      res.text.should.equal(line);
      res.lineType.should.equal(LineType.Suite);
      if (res.lineType === LineType.Suite) {
        res.indent.should.equal("  ");
        res.name.should.equal(suiteName);
      }
    });
  });

  // Run tests with the lower level Test.match AND with Classifier.next
  const testMatchers = [
    { id: "Test",       match: Test.match },
    { id: "Classifier", match: matchClassifer }
  ];

  parameterizedSuite('Classify "Test" Tests', testMatchers, (p) => {
    it("should not match empty", () => {
      should(Test.match("", pType)).equal(null);
    });

    it('should not match a "Suite"', () => {
      const suiteName = "This is a suite name";
      const line = mkSuite(suiteName, 0, cfg);
      const res = Test.match(line, pType);
      should(res).equal(null);
    });

    it("should match passing", () => {
      const line = mkTest(testPass, 1, cfg); // indent=1 -> 2 spaces + min indent
      const res = p.match(line, pType);
      should(res).not.equal(null);
      if (!res) return;

      res.text.should.equal(line);
      res.lineType.should.equal(LineType.Test);
      if (res.lineType === LineType.Test) {
        res.indent.should.equal("    ");
        res.name.should.equal(testPass.title);
        res.status.should.equal("pass");
        should(res.failNumber).equal(undefined);
        should(res.durationInMs).equal(undefined);
      }
    });

    it("should match passing with empty test name", () => {
      const line = mkTest({...testPass, title: ""}, 0, cfg);
      const res = p.match(line, pType);
      should(res).not.equal(null);
      if (!res) return;

      res.text.should.equal(line);
      res.lineType.should.equal(LineType.Test);
      if (res.lineType === LineType.Test) {
        res.indent.should.equal("  ");
        res.name.should.equal("");
        res.status.should.equal("pass");
        should(res.failNumber).equal(undefined);
        should(res.durationInMs).equal(undefined);
      }
    });

    it("should match failing", () => {
      const line = mkTest(testFail, 1, cfg); // indent=1 -> 2 spaces + min indent
      const res = p.match(line, pType);
      should(res).not.equal(null);
      if (!res) return;

      res.text.should.equal(line);
      res.lineType.should.equal(LineType.Test);
      if (res.lineType === LineType.Test) {
        res.indent.should.equal("    ");
        res.name.should.equal(testFail.title);
        res.status.should.equal("fail");
        should(res.failNumber).equal(1);
        should(res.durationInMs).equal(undefined);
      }
    });

    it("should match slow", () => {
      const line = mkTest(testSlow, 5, cfg); // indent=5 -> 10 spaces + 2 min
      const res = p.match(line, pType);
      should(res).not.equal(null);
      if (!res) return;

      res.text.should.equal(line);
      res.lineType.should.equal(LineType.Test);
      if (res.lineType === LineType.Test) {
        res.indent.should.equal("            ");
        res.name.should.equal(testSlow.title);
        res.status.should.equal("pass");
        should(res.failNumber).equal(undefined);
        should(res.durationInMs).equal(1234);
      }
    });

    it("should match pending", () => {
      const line = mkTest(testPending, 1, cfg); // indent=2 -> 2 spaces + 2 min
      const res = p.match(line, pType);
      should(res).not.equal(null);
      if (!res) return;

      res.text.should.equal(line);
      res.lineType.should.equal(LineType.Test);
      if (res.lineType === LineType.Test) {
        res.indent.should.equal("    ");
        res.name.should.equal(testPending.title);
        res.status.should.equal("pending");
        should(res.failNumber).equal(undefined);
        should(res.durationInMs).equal(undefined);
      }
    });
  });

  // Run tests with the lower level Output.match AND with Classifier.next
  const outputMatchers = [
    { id: "Output",     match: Output.match },
    { id: "Classifier", match: matchClassifer }
  ];

  parameterizedSuite('Classify "Output" Tests', outputMatchers, (p) => {

    it("should match empty", () => {
      const line = "";
      const res = p.match(line, pType);
      should(res).not.equal(null);
      if (!res) return;

      res.text.should.equal(line);
      res.lineType.should.equal(LineType.Output);
    });

    it("should match some text", () => {
      const line = "some random output";
      const res = p.match(line, pType);
      should(res).not.equal(null);
      if (!res) return;

      res.text.should.equal(line);
      res.lineType.should.equal(LineType.Output);
    });
  });

  // Run tests with the lower level Summary.match AND with Classifier.next
  const summaryMatchers = [
    { id: "Summary",    match: Summary.match },
    { id: "Classifier", match: matchClassifer }
  ];

  parameterizedSuite('Classify "Summary" Tests', summaryMatchers, (p) => {

    it("should match passing", () => {
      const line = mkEpilogue({passes: 1, duration: 24}, cfg);
      const res = p.match(line, pType);
      should(res).not.equal(null);
      if (!res) return;

      res.text.should.equal(line);
      res.lineType.should.equal(LineType.Summary);
      if (res.lineType === LineType.Summary) {
        res.count.should.equal(1);
        res.status.should.equal(TestStatus.pass);
        should(res.durationInMs).equal(24);
      }
    });

    it("should translate to milliseconds", () => {
      const line = mkEpilogue({passes: 1, duration: 1000 * 60 * 60}, cfg);
      // Check to ensure Epilogue translated time to an hour
      line.includes("1h").should.be.True();

      const res = p.match(line, pType);
      should(res).not.equal(null);
      if (!res) return;

      res.text.should.equal(line);
      res.lineType.should.equal(LineType.Summary);
      if (res.lineType === LineType.Summary) {
        res.count.should.equal(1);
        res.status.should.equal(TestStatus.pass);
        should(res.durationInMs).equal(1000 * 60 * 60);
      }
    });
  });

});
