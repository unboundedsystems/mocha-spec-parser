/*
 * Portions of this file are Copyright (c) 2011-2018 JS Foundation and contributors, https://js.foundation
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * 'Software'), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import ms = require("mocha/lib/ms");
import { Colors, Config, defaultConfig } from "../spec-info";

export interface Failure {
    name: string;
    message?: string;
    stack?: string;
    failNum?: number;
}
export interface Stats {
    passes: number;
    duration: number;
    pending?: number;
    failures?: Failure[];
}

/**
 * Color `str` with the given `type`,
 */
export function color(type: keyof Colors, str: string,
                      colors: Colors | null) {
    if (!colors) return str;
    return "\u001b[" + colors[type] + "m" + str + "\u001b[0m";
}

export function CR(config: Config) {
    if (config.isatty) {
        return "\u001b[2K\u001b[0G";
    }
    return "\r";
}

export function mkSuite(
    name: string,
    indent: number,
    config = defaultConfig,
) {
    const i = " ".repeat(indent * config.indentSize);
    return color("suite", i + name, config.colors) + config.lineEnd;
}

export interface TestResult {
    title: string;
    status: "pass" | "fail" | "pending";
    speed: "slow" | "medium" | "fast";
    duration: number;
    failNumber?: number;
}

export function mkTest(
    result: TestResult,
    indent: number,
    config = defaultConfig,
) {
    let ret = "";
    const i = " ".repeat(indent * config.indentSize);

    switch (result.status) {
        case "pending":
            ret = i + color("pending", `  - ${result.title}`, config.colors);
            break;

        case "pass":
            if (config.mocha2cr) {
                ret += CR(config);
            }
            ret +=
                i +
                color("checkmark", "  " + config.symbols.ok, config.colors) +
                color("pass", ` ${result.title}`, config.colors);
            if (result.speed !== "fast") {
                ret += color(result.speed, ` (${result.duration}ms)`,
                             config.colors);
            }
            break;

        case "fail":
            if (result.failNumber === undefined) {
                throw new Error('Must provide failNumber for a "fail" test case');
            }
            if (config.mocha2cr) {
                ret += CR(config);
            }
            ret +=
                i +
                color("fail", `  ${result.failNumber}) ${result.title}`,
                      config.colors);
            break;

        default:
            throw new Error(`mkTest: status '${result.status} not understood`);
    }
    ret += config.lineEnd;
    return ret;
}

export function mkEpilogue(
    stats: Stats,
    config = defaultConfig,
) {
    let ret = config.lineEnd;

    ret +=
        color("bright pass", " ", config.colors) +
        color("green", ` ${stats.passes || 0} passing`, config.colors) +
        color("light", ` (${ms(stats.duration)})`, config.colors) +
        config.lineEnd;

    if (stats.pending) {
        ret += color("pending", " ", config.colors) +
            color("pending", ` ${stats.pending} pending`, config.colors) +
            config.lineEnd;
    }

    // failures
    if (stats.failures) {
        let footnotes = "";

        let failNum = 1;
        for (const fail of stats.failures) {
            // If a particular failure has a failNum, use that. But continue
            // incrementing the failNum. This allows an epilogue to start
            // at something other than one or skip numbers without having to
            // ALWAYS put a failNum into every failure.
            if (fail.failNum !== undefined) failNum = fail.failNum;
            footnotes +=
                color("error title",
                      `  ${failNum}) ${fail.name}:${config.lineEnd}`,
                      config.colors) +
                color("error message",
                      `     ${fail.message || ""}`,
                      config.colors) +
                color("error stack",
                      `${config.lineEnd}${fail.stack || ""}${config.lineEnd}`,
                      config.colors) +
                config.lineEnd;
            failNum++;
        }

        ret += color("fail", `  ${failNum - 1} failing`, config.colors) +
            config.lineEnd +
            config.lineEnd +
            footnotes +
            config.lineEnd;
    }

    ret += config.lineEnd;
    return ret;
}
